React/ Redux/ TS Test Application goal is to demonstarate typical code structure, unidirectional data flow, react components composition.
 
Single Page Web application stores movies info locally. It supports such functionality as 'add item', 'remove Item', 'show movie details 
as a separate page', 'sort items in descending / ascending order'.
 
All data is persistent and stored in the browser Local Storage.