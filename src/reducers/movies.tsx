import { MovieAction }  from '../actions/movies';

import { ADD_MOVIE, DELETE_MOVIE, GET_MOVIES } from '../constants';
import { MovieState }  from '../types'


const initialSate = {
    movieList: []
};

export default (state: MovieState = initialSate, action: MovieAction) :MovieState => {
    switch(action.type) {

        case GET_MOVIES: {
            const movieList = action.movieList;
            return {
                ...state,
                movieList
            }
        }

        case ADD_MOVIE: {
            const movieList = state.movieList.slice();
            movieList.push(action.movie);
            return {
                ...state,
                movieList
            }
        }

        case DELETE_MOVIE: {
            const movieList = state.movieList.slice();
            movieList.splice(action.id, 1);
            return {
                ...state,
                movieList
            }
        }

        default:
            return state;
    }
}
