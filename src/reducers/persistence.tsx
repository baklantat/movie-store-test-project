import { PersistenceAction }  from '../actions/persistence';

import { INIT_STORAGE } from '../constants';
import { PersistenceState }  from '../types'


const initialSate = {
    isInit: false
};

export default (state: PersistenceState = initialSate, action: PersistenceAction) :PersistenceState => {
    switch(action.type) {

        case INIT_STORAGE: {
            return {
                ...state,
                isInit: action.isInit
            }
        }
        //
        // case DELETE_MOVIE: {
        //     const movieList = state.movieList;
        //     movieList.splice(action.id, 1);
        //     return {
        //         ...state,
        //         movieList
        //     }
        // }

        default:
            return state;
    }
}
