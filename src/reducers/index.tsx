import { combineReducers } from 'redux';
import movies from './movies';
import persistence from './persistence';

const rootReducer = combineReducers({
    persistence,
    movies
});

export default rootReducer;