// movie constants
export const ADD_MOVIE = 'ADD_MOVIE';
export type ADD_MOVIE = typeof ADD_MOVIE;

export const DELETE_MOVIE = 'DELETE_MOVIE';
export type DELETE_MOVIE = typeof DELETE_MOVIE;

export const GET_MOVIES = 'GET_MOVIES';
export type GET_MOVIES = typeof GET_MOVIES;

// persistence constants
export const UPDATE_STORAGE = 'UPDATE_STORAGE';
export type UPDATE_STORAGE = typeof UPDATE_STORAGE;

export const INIT_STORAGE = 'INIT_STORAGE';
export type INIT_STORAGE = typeof INIT_STORAGE;

