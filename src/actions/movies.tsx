import * as constants from '../constants';
import { Movie} from '../types'

export interface AddMovie {
    type: constants.ADD_MOVIE;
    movie: Movie
}

export interface DeleteMovie {
    type: constants.DELETE_MOVIE;
    id: number
}

export interface GetMovies {
    type: constants.GET_MOVIES;
    movieList: Movie[]
}

export type MovieAction = AddMovie | DeleteMovie  | GetMovies;

export const addMovie = (movie :Movie):AddMovie => {
    return {
        type: constants.ADD_MOVIE,
        movie
    }
};

export const deleteMovie = (id :number):DeleteMovie => {
    return {
        type: constants.DELETE_MOVIE,
        id
    }
};

export const getMovies = (movieList: Movie[]) :GetMovies => {
    return {
        type: constants.GET_MOVIES,
        movieList
    }
};