import * as constants from '../constants';
import { Movie} from '../types'

export interface InitStorage {
    type: constants.INIT_STORAGE;
    isInit: boolean;
}


export interface UpdateStorage {
    type: constants.UPDATE_STORAGE;
    movieList: Movie[];
}

export type PersistenceAction = UpdateStorage
    | InitStorage;

export const initStorage = () :InitStorage => {
    const isInit = true;
    return {
        type: constants.INIT_STORAGE,
        isInit
    }
};

export const upDateStorage = (movieList: Movie[]) :UpdateStorage => {
    return {
        type: constants.UPDATE_STORAGE,
        movieList
    }
};