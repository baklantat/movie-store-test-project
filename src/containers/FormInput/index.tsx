import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { Fields, ErrorFields}  from '../MovieDialog/';
interface FormInputProps extends Fields{
    onChange: (title : any) => any,
    errors: ErrorFields
}

const ErrorMsg = (errorMsg :string) => {
    return <div className="error">{errorMsg}</div>
};

export const FormInput = (props :FormInputProps) => {
    const { onChange, title, format, year, stars, errors} = props;
    return (
            <div>
            <div>
                <div>
                    <TextField
                        required={true}
                        margin="dense"
                        id="title"
                        value={title}
                        label="Movie title"
                        type="text"
                        fullWidth={true}
                        onChange={onChange('title')}
                    />
                    {errors.title  && ErrorMsg(errors.title)}
                </div>
                <div>
                    <TextField
                        required={true}
                        inputProps={{min: 1900, max: 2018}}
                        margin="dense"
                        id="year"
                        value={year}
                        label="Release Year (1900 - 2018)"
                        type="number"
                        fullWidth={true}
                        onChange={onChange('year')}
                    />
                    {errors.year  && ErrorMsg(errors.year)}
                    {errors.yearRange  && ErrorMsg(errors.yearRange)}

                </div>

            </div>
            <div>
                <div>
                    <TextField
                        required={true}
                        select={true}
                        margin="dense"
                        id="format"
                        value={format}
                        label="Format"
                        type="text"
                        fullWidth={true}
                        onChange={onChange('format')}
                    >
                        {formatList.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                    {errors.format  && ErrorMsg(errors.format)}
                </div>
                <div>
                    <TextField
                        required={true}
                        margin="dense"
                        id="stars"
                        value={stars}
                        label="Stars"
                        type="text"
                        fullWidth={true}
                        onChange={onChange('stars')}
                    />
                    {errors.stars  && ErrorMsg(errors.stars)}
                </div>
            </div>
        </div>
    )
};

const formatList = [
    {
        value: 'DVD',
        label: 'DVD',
    },
    {
        value: 'Blue-Ray',
        label: 'Blue-Ray',
    },
    {
        value: 'VHS',
        label: 'VHS',
    },
];