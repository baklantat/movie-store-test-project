import * as React from 'react';
import { Link } from 'react-router-dom';
import * as storage from '../../persistence/index';
import { mockedMovies } from '../../mock';
import { Movie } from '../../types';
import MovieDialog  from '../MovieDialog/index';
import Button from '@material-ui/core/Button';

import SortForm from '../../components/SortForm/index';
import './index.css';

export interface Props {
    isInit: boolean,
    movieList: Movie[],
    localStorageInit: () => void,
    getMovies: (movieList: Movie[]) => void,
    onAdd: (movie: Movie) => void,
    onDelete: (id: number) => void
}

interface DeleteProps extends Props{
    index: number
}

interface OpenDialogProps {
    onOpen: () => void,
    className: string
}

interface State {
    open: boolean
}

const AddIcon = ({onOpen, className} :OpenDialogProps) => {
  return (<div className={className} onClick={onOpen}>
      <span className='plusButton'> + </span>
  </div>)
};

const DeleteIcon = (props :DeleteProps) => {
    const { onDelete, movieList, index } = props;
    const deleteMovie = (): void => {
      onDelete(index);
      movieList.splice(index, 1);
      storage.updateMovies(movieList);
    };
    return (
      <div>
          <Button color="secondary" onClick={deleteMovie}> Delete </Button>
      </div>
    )
};

class MovieList  extends React.Component<Props, State> {

     constructor(props :Props) {
        super(props);
        this.toggleDialog = this.toggleDialog.bind(this);
        this.sort = this.sort.bind(this);
        this.state = {
             open: false
         }
    }

    public componentDidMount() {
        this.syncWithLocalStorage();
    }

    public toggleDialog() {
        this.setState({open: !this.state.open});
    }

    public sort(order :string) {
         this.sortByTitle(order);
    }

    public render() {
        const movies = this.props.movieList.map((movie :Movie, index :number) => {
            return (
                <li key={index.toString()}>
                    <Link to={{pathname: movie.title, state: movie}} >
                        {movie.title}
                    </Link>
                    <DeleteIcon {...{...this.props, index}}/>
                </li>
            )
        });
        return (
            <div className='container'>
                <div className='header-container'>
                    <h1 className='header-container__item'>Movie List</h1>
                    <AddIcon className='header-container__item' onOpen={this.toggleDialog}/>
                    <SortForm className='header-container__item' sort={this.sort}/>
                </div>

                <ul>
                    {movies}
                </ul>
                <div>
                    <MovieDialog
                        onAdd={this.props.onAdd}
                        open={this.state.open}
                        onClose={this.toggleDialog}
                        movieList={this.props.movieList}/>
                </div>
            </div>
        )
    }

    private syncWithLocalStorage() {
        if (!storage.isInit()) {
            storage.initStorage();
            storage.updateMovies(mockedMovies);
            this.props.localStorageInit();
        }
        this.props.getMovies(storage.getMovies());
    }

    private sortByTitle(order : string) {
         order === 'desc' ?
             this.props.movieList.sort((a,b) => a.title.toLowerCase() > b.title.toLowerCase() ? -1 : 1) :
             this.props.movieList.sort((a,b) => a.title.toLowerCase() < b.title.toLowerCase() ? -1 : 1);

         storage.updateMovies(this.props.movieList);
         this.props.getMovies(storage.getMovies());
    }
}

export default MovieList;