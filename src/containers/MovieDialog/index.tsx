import * as React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { FormInput } from '../FormInput';
import { validation } from '../../validators'
import {Movie} from "../../types";
import * as storage from "../../persistence";

export interface State extends Fields {
    errors: ErrorFields,
    isFormValid: boolean
}

export interface ErrorFields extends Fields{
    yearRange: string
}

export interface Fields {
    title: string,
    year: string,
    format: string,
    stars: string,
}

interface Props {
    onAdd: (movie: Movie) => void,
    onClose: () => void,
    open: boolean,
    movieList: Movie[]
}


export default class MovieDialog extends React.Component<Props, State> {

    public state: State = {
        title: '',
        year: '',
        format: '',
        stars: '',
        isFormValid: true,
        errors: {
            title: '',
            year: '',
            format: '',
            yearRange: '',
            stars: ''
        }
    };

    public onChange = (prop: string) => (event: any) => {
       this.handleInputChange(prop, event);
   };

    public render() {
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Add movie</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To update the movie list, please fill in all the data below.
                        </DialogContentText>
                        <FormInput
                            onChange={this.onChange}
                            title={this.state.title}
                            year={this.state.year}
                            format={this.state.format}
                            stars={this.state.stars}
                            errors={this.state.errors}
                            />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            Submit
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }

    private handleSubmit = () => {
        if (this.validateForm()) {
            const newMovie = {
                title: this.state.title,
                year: this.state.year,
                format: this.state.format,
                stars: this.state.stars
            };
            this.props.onAdd(newMovie);
            this.props.movieList.push(newMovie);
            storage.updateMovies(this.props.movieList);
            this.handleClose();
        }
    };

    private handleClose = () => {
        this.clearForm();
        this.props.onClose();
    };

    private validateForm = () => {
        const { errors, isFormValid } = validation(this.state);
        if (!isFormValid) {
            this.setState({errors});
        }
        return isFormValid;
    };

    private clearForm = () => {
        this.setState({
            title: '',
            year: '',
            format: '',
            stars: ''
        })
    };


    private handleInputChange = (prop: string, event: any) => {
        if (prop === 'title') { this.setState({[prop]: event.target.value}); }
        if (prop === 'year') { this.setState({[prop]: event.target.value}); }
        if (prop === 'format') { this.setState({[prop]: event.target.value});}
        if (prop === 'stars') { this.setState({[prop]: event.target.value}); }
    }
}
