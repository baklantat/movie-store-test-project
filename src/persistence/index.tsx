import { Movie } from '../types';

export const isInit = ():boolean => {
    return JSON.parse(localStorage.getItem('isInit') || 'false');
};

export const initStorage = ():void => {
    localStorage.setItem('isInit', 'true');
};

export const updateMovies = (movieList: Movie[]):void => {
    localStorage.setItem('movieList', JSON.stringify(movieList));
};

export const getMovies = ():Movie[] => {
  return JSON.parse(localStorage.getItem('movieList') || '{}');
};