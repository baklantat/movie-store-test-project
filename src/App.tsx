import * as React from 'react';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';

import MovieList from './connectors/MovieList';
import MovieDetails from './components/MovieDetails';


class App extends React.Component {
  public render() {
    return (
      <Router>
        <div>
            <Route exact={true} path="/" component={MovieList}/>
            <Route path="/:id" component={MovieDetails}/>
        </div>
      </Router>
    );
  }
}

export default App;
