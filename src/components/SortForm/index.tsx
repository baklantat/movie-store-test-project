import * as React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const sortOrder = [
    {
        value: 'asc',
        label: 'asc',
    },
    {
        value: 'desc',
        label: 'desc',
    },

];

interface Props {
    sort: (order: string) => void,
    className: string
}

interface State {
    sort: string
}

class SortForm extends React.Component<Props, State>{
    public state: State = {
       sort: ''
    };

    public handleChange = (name :any) => (event: any) => {
        this.setState({
            ['sort']: event.target.value,
        });

        this.props.sort(event.target.value);
    };

    public render() {
        return (
            <form className={this.props.className}>
                <TextField
                    select={true}
                    margin="dense"
                    id="sort"
                    value={this.state.sort}
                    helperText="Sort movies by alphabet"
                    type="text"
                    fullWidth={true}
                    onChange={this.handleChange('sort')}
                >
                    {sortOrder.map(option => (
                        <MenuItem key={option.value} value={option.value}>
                            {option.label}
                        </MenuItem>
                    ))}
                </TextField>
            </form>
        );
    }
}

export default SortForm;