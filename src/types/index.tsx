export interface Store {
    persistence: PersistenceState,
    movies: MovieState
    test: TestState
}

export interface TestState {
    count: number
}

export interface MovieState {
    movieList: Movie[]
}

export interface PersistenceState {
    isInit: boolean
}

export interface Movie {
    title: string,
    year: string,
    format: string,
    stars: string
}