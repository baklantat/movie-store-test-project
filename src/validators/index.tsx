import { Fields, ErrorFields,  State as FormState } from '../containers/MovieDialog/index';

export interface InputRange {
    max: number,
    min: number
}

const range : InputRange = {
    min: 1900,
    max: 2018
};

export const validation = ({title, year, format, stars}: FormState) => {
    const errors = {
        title: '',
        year: '',
        yearRange: '',
        format: '',
        stars: ''
    },
        isFormValid = true;

    return validateFields({title, year, format, stars}, errors, isFormValid);
};

const validateFields = (inputFields: Fields, errors: ErrorFields, isFormValid: boolean) => {
    const requiredFieldErrorMsg = 'This field is required',
          yearRangeErrorMsg = 'Invalid year',
          year = Number(inputFields.year);

    for (const prop in inputFields) {
        if (!inputFields[prop].trim()) {
            errors[prop] = requiredFieldErrorMsg;
            isFormValid = false;
        }
    }

    if (!errors.year && (year > range.max  || year < range.min)) {
        errors.yearRange = yearRangeErrorMsg;
        isFormValid = false;
    }
    return {errors, isFormValid}
};
