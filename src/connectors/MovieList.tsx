import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as movies from '../actions/movies';
import * as persistence from '../actions/persistence';
import  MovieList  from '../containers/MovieList';
import { Store, Movie } from '../types';

export function mapStateToProps(state :Store) {
    return {
        movieList: state.movies.movieList,
        isInit: state.persistence.isInit
    };
}


export function mapDispatchToProps(dispatch: Dispatch) {
    return {
        onAdd: (movie :Movie) => dispatch(movies.addMovie(movie)),
        onDelete: (id :number) => dispatch(movies.deleteMovie(id)),
        localStorageInit: () => dispatch(persistence.initStorage()),
        getMovies: (moviesList: Movie[]) => dispatch(movies.getMovies(moviesList))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieList as any);